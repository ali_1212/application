import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { SongsComponent } from './songs/songs.component';
import { CartComponent } from './cart/cart.component';
import { ShareDataService } from './share-data.service';


const routes: Routes = [
  {path: '', redirectTo: '/songs', pathMatch: 'full'},
  {path: 'songs', component: SongsComponent },
  {path: 'cart', component: CartComponent }
];

// const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
//   suppressScrollX: true
// };

@NgModule({
  declarations: [
    AppComponent,
    SongsComponent,
    CartComponent
  ],
  imports: [
    BrowserModule, RouterModule.forRoot(routes), 
  ],
  providers: [ShareDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
