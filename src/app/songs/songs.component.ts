import { Component, OnInit } from '@angular/core';

import { ShareDataService } from '../share-data.service';
import { Router } from '@angular/router';

let selectedSongsListGlobal = [];

@Component({
  selector: 'app-songs',
  templateUrl: './songs.component.html',
  styleUrls: ['./songs.component.css']
})
export class SongsComponent implements OnInit {

   songsList = ['Never Gonna Give You Up', 'Sandstorm', 'Tubthumping', 'Bohemian Rhapsody', 'Purple Haze', 'Wonderwall', 'Under The Bridge'];
   
   selectedSongsList;
  
  constructor(private shareData: ShareDataService, private nav: Router) { 

    this.selectedSongsList = selectedSongsListGlobal;
    selectedSongsListGlobal = this.shareData.updateSelections();
    
    
  }

  ngOnInit() {
    
  }

  addSong(song) {
    selectedSongsListGlobal.push(song);
    this.selectedSongsList = selectedSongsListGlobal;
  }

  sortAZ() {
    this.songsList.sort();
  }



  gotoCart() {
    
    this.shareData.saveData(selectedSongsListGlobal);
    this.nav.navigate(['/cart'])
  }

}
