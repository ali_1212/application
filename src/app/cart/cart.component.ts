import { Component, OnInit } from '@angular/core';
import { ShareDataService } from '../share-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  
  cartItems: Array<String> = [];
 
 constructor(private shareData: ShareDataService, private nav: Router) { 

  this.cartItems = this.shareData.data;
   
 }

 ngOnInit() {
  
  
   
 }

 gotoSongs() {
  this.nav.navigate(['/songs']);
 }

 delete(song: string) {
   this.shareData.deleteSong(this.shareData.data.indexOf(song))
 }

 

 
   
 }

