import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ShareDataService {

  data: Array<String>  = [];

  constructor() { }

  saveData(data: Array<String>) {
    console.log(data)
    if(data.length !== 0) {
      this.data= data;
    }
    
  }

  deleteSong(songIndex: number) {
    this.data.splice(songIndex, 1)
    console.log(this.data)
  }

  updateSelections() {
    return this.data;
  }
 
}
